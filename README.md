# Argon2

A dead simple utility for hashing and validating argon2 passwords.

This is just a wrapper around the standard go argon2 package.

## Usage

```
password := "hello world"

hashed, _ := HashPassword(password, &Params{
    Variant:     Variant2I,
    SaltLen:     16,
    Memory:      32 * 1024,
    Time:        2,
    Parallelism: 2,
    KeyLen:      32,
})

valid, _ := ValidatePassword("hello world", hashed)
```

## Installation

```
go get gitlab.com/projet-iota/go/argon2
```

## License

This project is licensed under the terms of the MIT license.
